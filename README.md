# Django-Project
Built this College Website in 2015 while being on IT-Club. Never Used Though. Models,URLs,Views are complete leaving templates part(some are completed.)
# Admin Link : http://subhanshu.pythonanywhere.com/admin #
# Username:Admin #
# Password:##235711## #
Python Version 3.5.1
Django Version 1.10.0
Database used sqlite3 (Default)
**Third Party Packages Used -** 
* Grappelli For Bootstrap Admin
* Pillow For Images 
*
Can also use Forms for further Integration, Email's for interior messaging.
Used Twitter Bootstrap for Front-End Development.*
```
 
## **Features:** ## 
* Full CMS.
* Notices,Department Notices,Upcoming Events,Department Login.
* All the images are separated/grouped by folders and images are renamed in this format: time stamp_filename.jpg/png/..
## **Steps for Running in local machine :** ##
* Download and extract the file
* Open the settings.py file and scroll down to change the PATH of Static files according to your machine. ( /home/.../mysite/static/  for unix/linux) or ( C:/Users/..../Desktop/mysite/mysite/static/)
* Run python manage.py migrate command.
* Runpython manage.py runserver command.
* Open browser and type 127.0.0.1:8000 to see the website .